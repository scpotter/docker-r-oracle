# r-oracle

## Motivation
Installing the required software and drivers to connect to an Oracle database
from R requires quite a lot of configuration and proved illusive on a
laptop.

## Description
This provides a Docker container based on Rockylinux capable of running both
R and Rstudio with the packages installed to connect to Oracle.

Tested only on ubuntu 20.04. The `docker run` command should work on Linux.
Modifications may be required for Mac.

## Usage
    # N.B. you may need to create files/tnsnames.ora,
    # either here (copying the example) or once in the container
    docker build -t docker-r-oracle .

    # Permit container connecting to the X server
    xhost +

    docker run --env=DISPLAY=$DISPLAY \
      --volume=/tmp/.X11-unix:/tmp/.X11-unix:rw \
      --rm -it docker-r-oracle

    # Using volumes for R libs and projects
    docker run \
      --volume=r-libs:/opt/R \
      --volume=r-projects:/home/ruser/projects \
      --rm docker-r-oracle bash -c 'sudo chmod 777 /opt/R && chown ruser:ruser /home/ruser/projects'
    docker run --env=DISPLAY=$DISPLAY --env=R_LIBS=/opt/R
      --volume=/tmp/.X11-unix:/tmp/.X11-unix:rw \
      --volume=r-libs:/opt/R \
      --volume=r-projects:/home/ruser/projects \
      --rm -it docker-r-oracle

    # In the container
    library(ROracle)
    drv <- dbDriver("Oracle")
    con <- dbConnect(drv = drv, username = DBUSER, password = DBPASS, dbname = HOST:PORT/DBNAME)
    # or con <- dbConnect(drv = drv, username = DBUSER, password = DBPASS, dbname = DBNAME)
    # Query etc

## Persistent storage
In order to save work between sessions, a volume will need to be added, either
as a named volume, or by mounting a folder on the host. You may also need
to create a volume that can be used for any installed packages.

## Maintenance
This container was created to solve a problem. As such it will be under minimal
maintenance, though additions such as other DB connectors and workarounds for
other operating systems are welcome.

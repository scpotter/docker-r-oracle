# This Dockerfile may not be optimal. I acknowledge the helpful instructions in various sites on the net.
# Any subsequent failures/errors are mine.
# https://www.cjvirtucio.co/posts/roracle-centos7/
# https://hub.docker.com/_/rockylinux
# https://computingforgeeks.com/install-r-rstudio-on-rocky-almalinux/
# https://www.golinuxcloud.com/install-r-on-rocky-linux/

FROM rockylinux:9

RUN dnf -y install epel-release
RUN dnf install https://dl.fedoraproject.org/pub/epel/epel-release-latest-9.noarch.rpm
RUN dnf config-manager --set-enabled crb

ENV ORACLE_INSTANT_CLIENT_VERSION=18.3
ENV ORACLE_YUM_URL=https://yum.oracle.com 
ENV ORACLE_HOME=/usr/lib/oracle/${ORACLE_INSTANT_CLIENT_VERSION}/client64
ENV ORACLE_YUM_REPO=public-yum-ol7.repo 
ENV ORACLE_YUM_GPG_KEY=RPM-GPG-KEY-oracle-ol7 
ENV OCI_INC=/usr/include/oracle/${ORACLE_INSTANT_CLIENT_VERSION}/client64 
ENV OCI_LIB=${ORACLE_HOME}/lib 
ENV LD_LIBRARY_PATH=/usr/local/lib:/usr/local/lib64

RUN rpm --import ${ORACLE_YUM_URL}/${ORACLE_YUM_GPG_KEY}
RUN curl -o /etc/yum.repos.d/${ORACLE_YUM_REPO} ${ORACLE_YUM_URL}/${ORACLE_YUM_REPO}
RUN sed -i 's/enabled=1/enabled=0/g' /etc/yum.repos.d/${ORACLE_YUM_REPO}
RUN yum config-manager --enable ol7_oracle_instantclient

RUN yum install -y libnsl sudo libaio-devel rlwrap procps-ng
RUN yum install -y oracle-instantclient18.3-basic oracle-instantclient18.3-sqlplus oracle-instantclient18.3-tools oracle-instantclient18.3-devel libcurl-devel
RUN echo "/usr/lib/oracle/18.3/client64/lib" | tee /etc/ld.so.conf.d/oracle.conf
RUN ldconfig 

RUN dnf -y install R
RUN R -e 'install.packages("ROracle", repos = "https://cran.uni-muenster.de/")'

# For Rstudio
RUN yum install -y make gcc perl-core pcre-devel wget zlib-devel
RUN curl -s https://www.openssl.org/source/openssl-1.1.0i.tar.gz | tar xzf -
RUN cd openssl-1.1.0i && ./config && make && make install && rm -rf openssl-1.1.0i
RUN wget https://download1.rstudio.org/desktop/rhel8/x86_64/rstudio-2022.07.2-576-x86_64.rpm
RUN yum -y localinstall ./rstudio-2022.07.2-576-x86_64.rpm && rm rstudio-2022.07.2-576-x86_64.rpm
RUN yum -y install mesa-libGLw mesa-dri-drivers unixODBC-devel oracle-instantclient18.3-sqlplus
RUN mkdir -p $ORACLE_HOME/network/admin
COPY files/tnsnames.ora* $ORACLE_HOME/network/admin/

RUN yum clean packages

# Give user sudo access
RUN useradd -ms /bin/bash ruser
RUN echo 'ruser ALL=(ALL:ALL) NOPASSWD:ALL' >> /etc/sudoers
USER ruser
WORKDIR /home/ruser
